import Vue from "vue";
import HelloComponent from "./components/Hello.vue";

let v = new Vue({
  el: "#app",
  template: `
    <div>
      Name: <input v-model="name" type="text">
      <hello-component :name="name" :initialEnthusiasm="5" />
    </div>
    `,
  data: { name: "World" },
  components: {
    HelloComponent
  }
});

let app1 = new Vue({
  el: "#app1",
  template: `
  <div> 
    <div><p>Name: <input v-model="name" type="text"></p></div>
    <div>=========================</div>
    <div><h1>Hello {{name}}!</h1></div>
  </div>`,
  data: {
    name: "World"
  }
});

let app2 = new Vue({
  el: "#app2",
  data: {
    message: "Hello World!"
  },
  methods: {
    reverseMessage: function () {
      this.message = this.message.split('').reverse().join('')
    } 
  }
})